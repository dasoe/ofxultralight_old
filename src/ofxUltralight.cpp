#include "ofxUltralight.h"

// -------------------------------
void ofxUltralight::setup() {
	ofSetLogLevel(OF_LOG_VERBOSE);
	//ofSetLogLevel(OF_LOG_WARNING);

	ofLogVerbose("setup ultralight");

	ultralight::Platform& platform = ultralight::Platform::instance();
	ultralight::Config config;
	config.face_winding = ultralight::kFaceWinding_CounterClockwise;
	config.device_scale_hint = 1.0;
	platform.set_config(config);

	ultralight::FontLoader* CreatePlatformFontLoader();
	ultralight::FileSystem* CreatePlatformFileSystem(const char* baseDir);
	 
	platform.set_gpu_driver(new ultralight::oeGPUDriver());
	
	config.font_family_standard = "Arial";

	ofLogVerbose("Creating Renderer...");
	_renderer = ultralight::Renderer::Create();

	ofLogVerbose("Creating View...");
	_view = _renderer->CreateView(200, 200, false);

	_view->set_load_listener(this);

	ofLogVerbose("load HTML in view...");
	_view->LoadHTML(R"(
  <html>
    <head>
      <style type="text/css">
      h1 { background: yellow; }
      h1, p { padding: 8px; text-align: center; }
      </style>
      <title>Hello World!</title>
    </head>
    <body>
      <h1>Hello World!</h1>
      <p>Welcome to Ultralight!</p>
    </body>
  </html>
  )");

	ofLogVerbose("looping till view is loaded...");
	while (!finished) {
		_renderer->Update();
		cout << finished << "-";
		std::this_thread::sleep_for(10ms);
	}
}

void ofxUltralight::update() {
	ofLogVerbose("update");
	_renderer->Update();
}

void ofxUltralight::draw() {
	ofLogVerbose("draw");
	_renderer->Render();
	//std::cout << "bitmap" << _view->bitmap()->width();
}