#pragma once

#define FRAMEWORK_PLATFORM_GLFW 1

#include "ofMain.h"

#include <Ultralight/Buffer.h>
#include <Ultralight/Renderer.h>
#include <Ultralight/platform/Platform.h>

#include <Ultralight/platform/Config.h>
#include <Ultralight/platform/GPUDriver.h>
#include <Ultralight/platform/FileSystem.h>
#include <Ultralight/platform/FontLoader.h>

namespace ultralight {

	class oeGPUDriver : public GPUDriver {
	public:
		oeGPUDriver() {}
		virtual ~oeGPUDriver() {}

		// Synchronization

		virtual void BeginSynchronize() {}

		virtual void EndSynchronize() {}

		// Textures

		virtual uint32_t NextTextureId() { return 0; }

		virtual void CreateTexture(uint32_t texture_id,
			Ref<Bitmap> bitmap) {}

		virtual void UpdateTexture(uint32_t texture_id,
			Ref<Bitmap> bitmap) {}

		virtual void BindTexture(uint8_t texture_unit,
			uint32_t texture_id) {}

		virtual void DestroyTexture(uint32_t texture_id) {}

		// Offscreen Rendering

		virtual uint32_t NextRenderBufferId() { return 0; }

		virtual void CreateRenderBuffer(uint32_t render_buffer_id,
			const RenderBuffer& buffer) {}

		virtual void BindRenderBuffer(uint32_t render_buffer_id) {}

		virtual void SetRenderBufferViewport(uint32_t render_buffer_id,
			uint32_t width,
			uint32_t height) {}

		virtual void ClearRenderBuffer(uint32_t render_buffer_id) {}

		virtual void DestroyRenderBuffer(uint32_t render_buffer_id) {}

		// Geometry

		virtual uint32_t NextGeometryId() { return 0; }

		virtual void CreateGeometry(uint32_t geometry_id,
			const VertexBuffer& vertices,
			const IndexBuffer& indices) {}

		virtual void UpdateGeometry(uint32_t geometry_id,
			const VertexBuffer& buffer,
			const IndexBuffer& indices) {}


		virtual void DrawGeometry(uint32_t geometry_id,
			uint32_t indices_count,
			uint32_t indices_offset,
			const GPUState& state) {}


		virtual void DestroyGeometry(uint32_t geometry_id) {}

		// Command Queue

		virtual void UpdateCommandList(const CommandList& queue) {
			std::cout << "[GPUDriver] UpdateCommandList: " << queue.size << "." << std::endl;
		}

		virtual bool HasCommandsPending() { return false; }

		virtual void DrawCommandList() {}
	};



}

class ofxUltralight : public ultralight::LoadListener {
	ultralight::RefPtr<ultralight::Renderer> renderer_;
	ultralight::RefPtr<ultralight::View> view_;
	bool done_ = false;
public:
	void setup();
	void update();
	void draw();

  ultralight::RefPtr<ultralight::View> _view;
  ultralight::RefPtr<ultralight::Renderer> _renderer;
  ultralight::GPUDriver* _driver;
  ultralight::GPUState _gpu_state;
  ultralight::Cursor _cur_cursor;

};

static bool finished = false;

class LoadListenerImpl : public ultralight::LoadListener {
public:
	virtual ~LoadListenerImpl() {}
	virtual void OnFinishLoading(ultralight::View* caller) {
		finished = true;
	}
};



