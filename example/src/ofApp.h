#pragma once

#include "ofMain.h"
#include "ofxUltralight.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();
		
		ofxUltralight ofxultralight;
};
