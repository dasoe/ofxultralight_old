#include "ofApp.h"

// This is a basic OF App setup.
// There's pre-defined functions. We only use 3 for clarity here. 
// setup() is executed once, update() and draw() in the loop
// later we add the other pre-defined functions like keypressed(), mousePressed() and so on

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetLogLevel(OF_LOG_VERBOSE);
	ofxultralight.setup();
}

//--------------------------------------------------------------
void ofApp::update(){
	ofxultralight.update();
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofxultralight.draw();
}