**-- This is a future addon. It's not working! --**


[Ultralight](https://github.com/ultralight-ux/ultralight) is a "lightweight, pure-GPU, HTML UI renderer for C\++". It's sort of a Successor to Awesomium. If we succeed to firmly inlcude it in openFrameworks, we can use the Power of HTML/CSS for layouting in a C++ App. From complex text to rich GUI interfaces there's many use cases.


**Status / Workflow**

Ultralight is in development, but Version already 1.0 is here! My skills in C++, Systems and GL are obviously not enough to build a working addon, so this repository only provides a *skeleton* for an OF App with non-working (and sometime deprecated) code for the Ultralight integration. I hope the pros from Ultralight can rather easily fill the gaps and repair the code to a *simple working state*. Delivering a texture would for example be enough.
I can then take over again and step after step integrate mouse actions and stuff with the openFrameworks community.
Find the appropriate openFrameworks forum post [here](https://forum.openframeworks.cc/t/embedded-browser-in-openframeworks-windows-vs/27841/6). 

**What it is**

I set up an OF addon for [openFrameworks](https://openframeworks.cc) 0.10.x with an actual (May 2019) version of ultralight. I am working on Windows. I did not include ultralight compiled on my machine, please compile yourself (or tell me if you want me to).
The addon comes with example code for an openFrameworks App, which includes the addon, which again includes Ultralight and uses some code based on the basic "Hello World" example that comes with ultralight.

Any help/hint is very much appreciated. 
If I can do more to make your work easier or if anything is unclear, please just let me know.

have a great day!
oe




**P.S.: necessary settings in VS when setting up the project anew**

you will need to do the following settings in VS (depending on what and how you include, you might add some more paths, but this should be the start)

++C\+\+: General: Additional Include Directories:++

..\..\..\addons\ofxUltralight\libs\ultralight\deps\WebCore\include
..\..\..\addons\ofxUltralight\libs\ultralight\deps\UltralightCore\include
..\..\..\addons\ofxUltralight\libs\ultralight\deps\Ultralight\include
..\..\..\addons\ofxUltralight\src
..\..\..\addons\ofxUltralight\libs\ultralight\deps\glfw\deps

++C\+\+: Language: Enable Run-Time Typ3 Information:++

No (/GR-)

++Linker: General: Additional Library Directories:++

..\..\..\addons\ofxUltralight\libs\ultralight\deps\Ultralight\lib
..\..\..\addons\ofxUltralight\libs\ultralight\deps\UltralightCore\lib
..\..\..\addons\ofxUltralight\libs\ultralight\deps\WebCore\lib

++Linker: Input: Additional Dependencies:++

Ultralight.lib
UltralightCore.lib
WebCore.lib

++copy to bin directory of your App:++

Ultralight.dll
UltralightCore.dll
WebCore.dll
